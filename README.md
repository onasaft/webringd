# Webringd
- A webring daemon written with the Mongoose web server library

### Dependencies
- `gcc`
- `libc6-dev`

## Installation
- clone this repo in `/var/www` or wherever really
- `make`
- use the systemd file to run it
- setup the nginx file to have it proxied

## Usage
- edit `list.txt` with the webring members domain names (https:// not needed)
- edit the port number or protocol in `main.c`

## Pages

This program produces three main pages for use as links:

- `/next` -- Go to the next site in the webring (from whichever site you are linked from).
- `/prev` -- Go to the previous site in the webring.
- `/rand` -- Go to a random site in the webring.

- `/` -- The index should be served as a static folder directly by nginx.

If linked to `/next` or `/prev` without a HTTP referer in the webring, they will function like `/rand`.
